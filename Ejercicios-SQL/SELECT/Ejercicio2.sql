/*
Exercise:
Write a statement that will select the City column from the Customers table.

____ ____ ____ Customers;

*/

SELECT City FROM Customers;
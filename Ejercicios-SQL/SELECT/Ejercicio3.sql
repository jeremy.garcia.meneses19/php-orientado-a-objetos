/*

Exercise:
Select all the different values from the Country column in the Customers table.

____ ____ Country FROM Customers;

*/

SELECT DISTINCT Country FROM Customers;
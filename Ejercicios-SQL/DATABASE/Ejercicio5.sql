/*

Exercise:
Use the TRUNCATE statement to delete all data inside a table.
 __________Persons;
*/

TRUNCATE TABLE Persons;
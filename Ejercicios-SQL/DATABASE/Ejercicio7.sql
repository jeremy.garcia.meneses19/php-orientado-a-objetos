/*
Exercise:
Delete the column Birthday from the Persons table.


 _____________Persons
 __________Birthday;

*/

ALTER TABLE Persons
DROP COLUMN Birthday;
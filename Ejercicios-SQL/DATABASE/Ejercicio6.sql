/*
Exercise:
Add a column of type DATE called Birthday.


 _______Persons
 ___________;

*/


 ALTER TABLE Persons
 ADD Birthday DATE;
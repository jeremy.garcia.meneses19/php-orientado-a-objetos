/*
Exercise:
Update the City value and the Country value.


______ Customers
__ City = 'Oslo' __
___ = 'Norway'
WHERE CustomerID = 32;
*/

UPDATE Customers
SET City = 'Oslo' ,
Country = 'Norway'
WHERE CustomerID = 32;
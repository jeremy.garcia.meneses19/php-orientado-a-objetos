/*
Exercise:
Update the City column of all records in the Customers table.


 _____ Customers
 ___ City = 'Oslo';
*/

UPDATE Customers
SET City = 'Oslo';


/*
Exercise:
Use the BETWEEN operator to select all the records where the value of the Price column is between 10 and 20.

SELECT * FROM Products
WHERE Price _________;

*/

SELECT * FROM Products
WHERE Price BETWEEN 10 and 20;
/*
Exercise:
Select all records where the value of the City column contains the letter "a".


SELECT * FROM Customers
__________________;
*/

SELECT * FROM Customers
WHERE City Like '%a%';
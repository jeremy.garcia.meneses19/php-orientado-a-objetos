/*
Exercise:
Select all records where the value of the City column starts with the letter "a".

SELECT * FROM Customers
____________;
*/

SELECT * FROM Customers
WHERE City Like 'a%';
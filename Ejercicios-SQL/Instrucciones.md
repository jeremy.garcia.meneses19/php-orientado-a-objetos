# Ejercicios de SQL


Para esta entrega deberás realizar los ejercicios del link adjunto como actividad de refuerzo:

enlace: https://www.w3schools.com/sql/exercise.asp

Una vez completados todos los ejercicios de la actividad (cuando todos estén marcados) toma una captura de pantalla y subela como evidencia a tu repositorio del curso.
/*
Exercise:
Select all records from the Customers table, sort the result alphabetically by the column City.

SELECT * FROM Customers
_____ ___;
*/

SELECT * FROM Customers ORDER BY City;
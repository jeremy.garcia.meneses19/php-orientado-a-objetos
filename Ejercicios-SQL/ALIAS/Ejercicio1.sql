/*
Exercise:
When displaying the Customers table, make an ALIAS of the PostalCode column, the column should be called Pno instead.


SELECT CustomerName,
Address,
PostalCode ______
FROM Customers;

*/

SELECT CustomerName,
Address,
PostalCode as Pno
FROM Customers;
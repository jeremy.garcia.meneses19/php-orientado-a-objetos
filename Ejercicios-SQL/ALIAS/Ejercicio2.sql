/*
Exercise:
When displaying the Customers table, refer to the table as Consumers instead of Customers.


SELECT *
FROM Customers __________;
*/

SELECT *
FROM Customers as Consumers;
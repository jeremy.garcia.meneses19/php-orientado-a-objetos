/*
Exercise:
Delete all the records from the Customers table where the Country value is 'Norway'.


 _______ Customers
 ___ Country = 'Norway';
*/


DELETE FROM Customers WHERE Country = 'Norway';
/*
Exercise:
Choose the correct JOIN clause to select all the records from the Customers table plus all the matches in the Orders table.


SELECT *
FROM Orders
__________________
ON Orders.CustomerID=Customers.CustomerID;

*/

SELECT *
RIGHT JOIN 
ON Orders.CustomerID=Customers.CustomerID;
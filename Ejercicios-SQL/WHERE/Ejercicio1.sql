/*
Exercise:
Select all records where the City column has the value "Berlin".

SELECT * FROM Customers
______ _______= _______;

*/
SELECT * FROM Customers
WHERE City = "Berlin";
/*
Exercise:
Select all records where the City column has the value 'Berlin' or 'London'.


_____ * FROM Customers
____ City = 'Berlin'
__ _____ = '____';

*/


SELECT * FROM Customers
WHERE City = 'Berlin'
OR City = 'London';

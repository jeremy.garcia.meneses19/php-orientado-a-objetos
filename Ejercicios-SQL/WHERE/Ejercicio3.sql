/*
Exercise:
Select all records where the CustomerID column has the value 32.


SELECT * FROM Customers
_____ CustomerID __ __;

*/


SELECT * FROM Customers
WHERE CustomerID = 32;

/*
Exercise:
List the number of customers in each country.


SELECT _________ (CustomerID),
Country
FROM Customers
________________________;
*/

SELECT 
COUNT (CustomerID), Country
FROM Customers
GROUP BY Country;
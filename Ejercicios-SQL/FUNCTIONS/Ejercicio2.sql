/*
Exercise:
Use an SQL function to select the record with the highest value of the Price column.


SELECT ___________
FROM Products;
*/

SELECT MAX(Price)
FROM Products;
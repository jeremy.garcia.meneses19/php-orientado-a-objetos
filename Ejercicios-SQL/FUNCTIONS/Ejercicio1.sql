/*
Exercise:
Use the MIN function to select the record with the smallest value of the Price column.

SELECT ______________
FROM Products;
*/

SELECT MIN(Price)
FROM Products;
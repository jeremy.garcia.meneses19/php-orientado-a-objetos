/*
Exercise:
Use an SQL function to calculate the average price of all products.

SELECT _________
FROM Products;
*/


SELECT AVG(Price)
FROM Products;
/*

Exercise:
Select all records from the Customers where the PostalCode column is empty.


SELECT * FROM Customers
WHERE _______ ___ ____;

*/

SELECT * FROM Customers
WHERE PostalCode IS NULL;
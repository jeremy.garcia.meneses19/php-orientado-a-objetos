/*
Exercise:
Select all records from the Customers where the PostalCode column is NOT empty.


SELECT * FROM Customers
WHERE ________ __ ___ ____;

*/

SELECT * FROM Customers
WHERE PostalCode IS NOT NULL;
/*

Insert a new record in the Customers table.


_______ Customers __ 
CustomerName, 
Address, 
City, 
PostalCode,
Country __
_______ __
'Hekkan Burger',
'Gateveien 15',
'Sandnes',
'4306',
'Norway' ___;

*/

INSERT INTO Customers (
CustomerName, 
Address, 
City, 
PostalCode,
Country )
VALUES (
'Hekkan Burger',
'Gateveien 15',
'Sandnes',
'4306',
'Norway' );
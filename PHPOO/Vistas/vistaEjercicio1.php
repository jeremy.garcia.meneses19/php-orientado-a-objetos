<?php
	//Incluimos los archivos de la clase Carro y de la clase Moto
    include('../Clases/ejercicio1/Carro.php');
    include('../Clases/ejercicio1/Moto.php');

?>

<!DOCTYPE html>
<html>
<head>

	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/bootstrap-grid.css">
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
	<title>
		Indice
	</title>
</head>
<body>
	
	<div class="container" style="margin-top: 4em">
	
	<header> <h1>Carro y Moto</h1></header><br>
	<form method="post">
		<div class="form-group row">

			 <label class="col-sm-3" for="CajaTexto1">Color del carro:</label>
			 <div class="col-sm-4">
					<input class="form-control" type="color" name="color" id="CajaTexto1">
			</div>
			<!--Sirve para distribui campos en filas -->
			<div class="col-sm-4">
			</div>


			<!-- Campo para atributo marca de la moto-->
            <label class="col-sm-3" for="CajaTexto1">Marca de la moto:</label>
			 <div class="col-sm-4">
					<input class="form-control" type="text" name="marca" id="CajaTexto2">
			</div>
			<!--Sirve para distribui campos en filas -->
			<div class="col-sm-4">
			</div>

			<!-- Campo para atributo peso de la moto-->
            <label class="col-sm-3" for="CajaTexto1">Peso de la moto (Kg):</label>
			 <div class="col-sm-4">
					<input class="form-control" type="text" name="peso" id="CajaTexto3" pattern = "[0-9]+" placeholder = "Ingresa un número">
			</div>
			<!--Sirve para distribui campos en filas -->
			<div class="col-sm-4">
			</div>

						
		</div>
		<button class="btn btn-primary" type="submit" >enviar</button>
		<a class="btn btn-link offset-md-8 offset-lg-9 offset-6" href="../index.php">Regresar</a>
	</form>
	</div>
	<br>
	<br>
	<!-- mesaje del servidor para eL Carro-->
	<input type="text" class="form-control" value="<?php  echo $mensajeServidor; ?>" readonly>
	<!-- mesaje del servidor para la Moto-->
    <input type="text" class="form-control" value="<?php  echo $mensajeServidor2; ?>" readonly>



</body>
</html>

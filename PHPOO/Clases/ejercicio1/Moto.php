<?php

//crea aqui la clase Moto junto con dos propiedades public
class Moto{
	//declaracion de propiedades
	public $marca;
	public $peso;
}

//crea aqui la instancia o el objeto de la clase Moto
$moto = new Moto;

//inicializamos el mensaje que lanzara el servidor con vacio
$mensajeServidor2='';

//verifica si se ha enviado una petición POST
 if (!empty($_POST)){

 	// recibe aqui los valores mandados por post y arma el mensaje para front 
	//almacenamos el valor mandado por POST en los atributos
	$moto->marca=$_POST['marca'];
	$moto->peso=$_POST['peso'];
	//se construye el mensaje que sera lanzado por el servidor
	$mensajeServidor2='Seleccionaste la marca: '.$_POST['marca'] . ' con un peso de: ' . $_POST['peso']. ' Kg';
 }  

?>

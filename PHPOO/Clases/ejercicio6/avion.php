<?php
include_once('transporte.php');

//declaracion de la clase hijo o subclase Avión
class avion extends transporte{
    //Declaración de atributos
    private $numero_turbinas;

    //sobreescritura de constructor
    public function __construct($nom,$vel,$com,$tur){
        parent::__construct($nom,$vel,$com);
        $this->numero_turbinas=$tur;
    }

    // sobreescritura de metodo
    public function resumenAvion(){
        $mensaje=parent::crear_ficha();
        $mensaje.='<tr>
                    <td>Numero de turbinas:</td>
                    <td>'. $this->numero_turbinas.'</td>				
                </tr>';
        return $mensaje;
    }
}

$mensajeAvion='';

if (!empty($_POST)){
switch ($_POST['tipo_transporte']) {
    case 'aereo':
        //creacion del objeto con sus respectivos parametros para el constructor
        $jet1= new avion('jet','400','gasoleo','2');
        $mensajeAvion=$jet1->resumenAvion();
        break;		
}

}



?>
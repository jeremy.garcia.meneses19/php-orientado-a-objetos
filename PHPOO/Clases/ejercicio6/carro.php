<?php  
include_once('transporte.php');

	//declaracion de la clase hijo o subclase Carro
	class carro extends transporte{
        //Declaración de atributos
		private $numero_puertas;

		//declaracion de constructor
		public function __construct($nom,$vel,$com,$pue){
			//sobreescritura de constructor de la clase padre
			parent::__construct($nom,$vel,$com);
			$this->numero_puertas=$pue;
				
		}

		// declaracion de metodo
		public function resumenCarro(){
			// sobreescribitura de metodo crear_ficha en la clse padre
			$mensaje=parent::crear_ficha();
			$mensaje.='<tr>
						<td>Numero de puertas:</td>
						<td>'. $this->numero_puertas.'</td>				
					</tr>';
			return $mensaje;
		}
	} 

    $mensajeCarro='';


    if (!empty($_POST)){
		// verificamos si la opción del formulario fue terrestre.
		switch ($_POST['tipo_transporte']) {
			case 'terrestre':
				//creacion del objeto con sus respectivos parametros para el constructor
				$carro1= new carro('carro','200','gasolina','4');
				$mensajeCarro=$carro1->resumenCarro();
				break;	
		}

    }    


?>
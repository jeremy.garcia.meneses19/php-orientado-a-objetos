<?php
include_once('transporte.php');

//declaracion de la clase hijo o subclase Espacial
class Espacial extends transporte{
    //Declaración de atributos
    private $num_tripulantes;
    private $energia_despegue;

    //sobreescritura de constructor
    public function __construct($nom,$vel,$com,$trip,$energia){
        parent::__construct($nom,$vel,$com);
        $this->num_tripulantes=$trip;
        $this->energia_despegue=$energia;
    }

    // sobreescritura de metodo
    public function resumenModuloLunar(){
        $mensaje=parent::crear_ficha();
        $mensaje.=
                '<tr>
                    <td>Número de tripulantes:</td>
                    <td>'. $this->num_tripulantes.'</td>				
                </tr>
                <tr>
                    <td> Energía necesaria para despegue: </td>
                    <td>'. $this->energia_despegue.'</td>
                </tr>';
                    
        return $mensaje;
    }
}

$mensajeModuloLunar='';    

if (!empty($_POST)){
    //verificamos si la opción del formulario fue espacial.
    switch ($_POST['tipo_transporte']) {
        case 'espacial':
            //creacion del objeto con sus respectivos parametros para el constructor
            $moduloLunar1= new Espacial('modulo lunar','600','gasoleo','2',650);
            $mensajeAvion=$moduloLunar1->resumenModuloLunar();
            break;		
    }
}



?>
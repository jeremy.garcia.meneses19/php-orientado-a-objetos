
<?php
include_once('transporte.php');

//declaracion de la clase hijo o subclase barco
class barco extends transporte{
        //Declaración de atributos
		private $calado;

		//sobreescritura de constructor
		public function __construct($nom,$vel,$com,$cal){
			parent::__construct($nom,$vel,$com);
			$this->calado=$cal;
		}

		// sobreescritura de metodo
		public function resumenBarco(){
			$mensaje=parent::crear_ficha();
			$mensaje.='<tr>
						<td>Calado:</td>
						<td>'. $this->calado.'</td>				
					</tr>';
			return $mensaje;
		}
	}


    $mensajeBarco='';


    if (!empty($_POST)){
	switch ($_POST['tipo_transporte']) {
		case 'maritimo':
			//creacion del objeto con sus respectivos parametros para el constructor
			$bergantin1= new barco('bergantin','40','na','15');
			$mensajeBarco=$bergantin1->resumenBarco();
			break;		
	}

    }    


	

?>
<?php
//creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;
	private $circula; //Almacena el resultado de la verificación

	//declaracion del método verificación (Manejando el año como un entero)
	//public function verificacion($anio_fab){
		//if ($anio_fab < 1990){
			//$this->circula = "No";
		//}elseif($anio_fab >= 1990 && $anio_fab <= 2010){
			//$this->circula = "Revisión";
		//}else{
			//$this->circula = "Si";
		//}
	//}

	//declaración del método verificación (Haciendo las comparaciones con strtotime)
	public function verificacion($fecha_fabricacion){
		if ($fecha_fabricacion < strtotime('1990-01')){
			$this->circula = "No";
		}elseif($fecha_fabricacion >= strtotime('1990-01') && $fecha_fabricacion <= strtotime('2010-12')){
			$this->circula = "Revisión";
		}else{
			$this->circula = "Si";
		}
	}

	//método getter para atributo privado circula
	public function get_circula(){
		return $this->circula;
	}


}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();

if (!empty($_POST)){
	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];
	
	//Lineas para realizar las comparaciones con fechas
	$fecha = date_create($_POST['anio_fab']); //Creamos un objeto DateTime
	$fecha_formato = date_format($fecha,"Y-m"); //Le damos el formato año-mes al objeto DateTime
	$fecha_fabricacion = strtotime($fecha_formato); //Convertimos la fecha en una fecha UNIX para comparar las fechas
	$Carro1->verificacion($fecha_fabricacion);


	//Obtenemos el año de fabricación de la fecha y lo casteamos a int
	//$anio = (int)(explode('-',$_POST['anio_fab'])[0]); Linea para realizar la comparación con el año como entero
	//$Carro1->verificacion($anio);
}





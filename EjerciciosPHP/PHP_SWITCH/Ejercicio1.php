<?php

/*
Exercise:
Add a section that will output "Neither" if $color is neither "red" nor "green".


switch ($color) {
  case "red":
    echo "Hello";
    break;
  case "green":
    echo "Welcome";
    break;
    _______
    echo "Neither";
}   

*/

switch ($color) {
    case "red":
      echo "Hello";
      break;
    case "green":
      echo "Welcome";
      break;
    default:
      echo "Neither";
  }  

?>
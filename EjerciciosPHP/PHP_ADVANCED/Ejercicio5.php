<?php

/*
Exercise:
Create a session variable named "favcolor".


session_start();
_______["favcolor"] = "green";
*/
session_start();
$_SESSION["favcolor"] = "green";

?>
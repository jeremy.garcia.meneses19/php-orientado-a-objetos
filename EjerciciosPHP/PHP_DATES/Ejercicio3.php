<?php

/*

Exercise:
Use the correct format parameter to output the time like this: 13:49:19 (with hour as a 24-hour format).


echo date(__________);

*/

echo date("H:i:s");

?>
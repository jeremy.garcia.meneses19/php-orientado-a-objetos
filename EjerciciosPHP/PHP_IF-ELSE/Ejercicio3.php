<?php

/*
Exercise:
Output "Yes" if $a is equal to $b, otherwise output "No".

$a = 50;
$b = 10;
__ ($a == $b) {
  echo "Yes";
} ____ {
  echo "No";
}

*/

$a = 50;
$b = 10;
if ($a == $b) {
  echo "Yes";
} else {
  echo "No";
}

?>
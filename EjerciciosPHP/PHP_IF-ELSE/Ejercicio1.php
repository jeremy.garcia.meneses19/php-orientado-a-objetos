<?php

/*

Exercise:
Output "Hello World" if $a is greater than $b.

$a = 50;
$b = 10;
 
__ ___ > ___{
  echo "Hello World";
}

*/


$a = 50;
$b = 10;
 
if ($a > $b) {
  echo "Hello World";
}


?>

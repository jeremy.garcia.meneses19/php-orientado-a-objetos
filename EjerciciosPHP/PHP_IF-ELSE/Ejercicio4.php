<?php

/*
Exercise:
Output "1" if $a is equal to $b, print "2" if $a is greater than $b, otherwise output "3".


$a = 50;
$b = 10;
__ ($a == $b) {
  echo "1";
} ___ ($a > $b) {
  echo "2";
} ____ {
      echo "3";
}

*/

$a = 50;
$b = 10;
if ($a == $b) {
  echo "1";
} elseif ($a > $b) {
  echo "2";
} else {
      echo "3";
}


?>
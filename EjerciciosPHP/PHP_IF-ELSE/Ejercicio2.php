<?php

/*

Exercise:
Output "Hello World" if $a is NOT equal to $b.


$a = 50;
$b = 10;
__ ____ ___ ___{
  echo "Hello World";
}

*/


$a = 50;
$b = 10;
 
if ($a != $b) {
  echo "Hello World";
}


?>

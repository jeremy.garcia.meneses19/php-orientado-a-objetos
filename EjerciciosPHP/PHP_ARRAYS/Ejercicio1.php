<?php
/*

Exercise:
Use the correct function to output the number of items in an array.


$fruits = array("Apple", "Banana", "Orange");
echo __________;

 */

$fruits = array("Apple", "Banana", "Orange");
echo count($fruits);
 
?>
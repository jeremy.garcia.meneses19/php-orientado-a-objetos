<?php
/*

Exercise:
Output the second item in the $fruits array.


$fruits = array("Apple", "Banana", "Orange");
echo _________;


 */

$fruits = array("Apple", "Banana", "Orange");
echo $fruits[1];

 
?>
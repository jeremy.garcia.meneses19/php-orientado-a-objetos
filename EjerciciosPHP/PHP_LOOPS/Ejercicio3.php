<?php

/*
Exercise:
Create a loop that runs from 0 to 9.


___ ($i = 0; $i < 10; ____) {
  echo $i;
}


*/

for ($i = 0; $i < 10; $i++) {
    echo $i;
}

?>
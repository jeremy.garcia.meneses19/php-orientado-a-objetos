<?php

/*
Exercise:
Output $i as long as $i is less than 6.


$i = 1; 

____ ($i < 6) _

  echo $i;
  $i++;
_


*/

$i = 1; 

while ($i < 6){

  echo $i;
  $i++;
}

?>
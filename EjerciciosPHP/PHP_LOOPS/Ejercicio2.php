<?php

/*

Exercise:
Output $i as long as $i is less than 6.


$i = 1; 

__ {
    echo $i;
    $i++;
} ____($i < 6);

*/

$i = 1; 

do{
    echo $i;
    $i++;
} while($i < 6);


?>
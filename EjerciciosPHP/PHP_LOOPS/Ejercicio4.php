<?php

/*
Exercise:
Loop through the items in the $colors array.


$colors = array("red", "green", "blue", "yellow"); 

______ ($colors ___ $x) {
  echo $x;
}


*/

$colors = array("red", "green", "blue", "yellow"); 

foreach ($colors as $x) {
  echo $x;
}


?>
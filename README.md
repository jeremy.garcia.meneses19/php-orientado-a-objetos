# PHP ORIENTADO A OBJETOS

En este repositorio se encuentran los ejercicios relacionados al curso PHP Orientado a Objetos.


## Descripción

Para este curso se necesitarán conocimientos previos en :
* Paradigma Orientado a Objetos
* HTML 5
    * Métodos de petición (GET y POST principalmente)
    * Creación de formularios
* CSS3 (elemental)
* PHP estructurado
    * Funciones para cadenas y arreglos
    * Conexión a base de datos
* SQL (elemental)
* Control de versiones con Git


Si aún necesitas reforzar estos temas puedes consultar estos enlaces:


HTML 5 y CSS
* https://learn.freecodecamp.org/responsive-web-design/basic-html-and-html5
* https://learn.freecodecamp.org/responsive-web-design/basic-css


Bootstrap (recomendable y opcional)
* https://learn.freecodecamp.org/front-end-libraries/bootstrap/


PHP estructurado y conexión a base de datos
* https://www.w3schools.com/php/
* https://www.w3schools.com/php/php_mysql_intro.asp


SQL
* https://www.w3schools.com/sql/


Control de versiones con Git
* https://rogerdudler.github.io/git-guide/index.es.html


## Autor
* García Meneses Jeremy
### Contacto
jeremy.garcia.meneses19@gmail.com

